import React, { useState } from "react";
import axios, { AxiosResponse } from "axios";

interface Item {
  id: string;
  name: string;
  url: string;
  snippet: string;
}

const App: React.FC = () => {
  const [searchQuery, setSearchQuery] = useState<string>("");
  const [searchResults, setSearchResults] = useState<Item[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [searchFieldTouched, setSearchFieldTouched] = useState<boolean>(false);

  const apiKey: string = "a19bf106e33d4ae181308b8d7b5b0079";

  const handleSearch = async (): Promise<void> => {
    // Check if searchQuery is empty
    if (!searchQuery.trim()) {
      setSearchFieldTouched(true);
      return;
    }

    setLoading(true);
    try {
      const response: AxiosResponse<any> = await axios.get(
        "https://api.bing.microsoft.com/v7.0/search",
        {
          params: {
            q: searchQuery,
          },
          headers: {
            "Ocp-Apim-Subscription-Key": apiKey,
          },
        }
      );
      setSearchResults(response.data.webPages.value);
    } catch (error: any) {
      console.error("Error fetching search results:", error);
      setSearchResults([]);
    } finally {
      setLoading(false);
    }
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      handleSearch();
    }
  };

  return (
    <div className="container mt-5">
      <h1>Bing Search</h1>
      <div className="input-group mb-3">
        <input
          type="text"
          className={`form-control iconified ${
            searchFieldTouched && !searchQuery.trim() ? "is-invalid" : ""
          }`}
          placeholder="Search..."
          value={searchQuery}
          onChange={(e) => {
            setSearchFieldTouched(true);
            setSearchQuery(e.target.value);
          }}
          onBlur={() => setSearchFieldTouched(true)}
          onKeyPress={handleKeyPress}
        />
        <div className="input-group-append">
          <button
            className="btn btn-primary"
            type="button"
            onClick={handleSearch}
            disabled={loading}
          >
            {loading ? "Searching..." : <i className="fas fa-search"></i>}
          </button>
        </div>
        {searchFieldTouched && !searchQuery.trim() && (
          <div className="invalid-feedback">This field is required.</div>
        )}
      </div>
      <ul className="list-group">
        {searchResults && searchResults.length > 0 ? (
          searchResults.map((item: Item) => (
            <li key={item.id} className="list-group-item">
              <a href={item.url} target="_blank" rel="noopener noreferrer">
                {item.name}
              </a>
              <p>{item.snippet}</p>
            </li>
          ))
        ) : (
          <li className="list-group-item">No search results found.</li>
        )}
      </ul>
    </div>
  );
};

export default App;
